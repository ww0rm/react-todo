import {createStore, combineReducers, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga'
import * as reducers from './reducers';
import sagas from './sagas';
const { logger } = require(`redux-logger`);

const saga = createSagaMiddleware();
let middlewares = [];

middlewares.push(saga);
middlewares.push(logger);

const store = createStore(combineReducers(reducers), {}, applyMiddleware(...middlewares));
saga.run(sagas);

export default store;