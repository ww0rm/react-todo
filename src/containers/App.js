import React, { Component } from 'react';
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import history from '../config/history'
import {Nav, Navbar, NavItem} from "react-bootstrap";

@connect(state => (state))
export default class App extends Component {
    componentDidMount () {
        if (!this.props.user.isLoggedIn) {
            history.replace('/auth');
        }
    };

    render() {
        return (
            <Navbar inverse collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand>
                        <Link to='/'>ToDO</Link>
                    </Navbar.Brand>
                    <Navbar.Toggle />
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav>
                        <Link to='/auth'>Auth</Link>
                        <Link to='/sdfsdf'>Ggfdgfd</Link>
                    </Nav>
                    <Nav pullRight>
                        <NavItem eventKey={1} href="#">
                            Link Right
                        </NavItem>
                        <NavItem eventKey={2} href="#">
                            Link Right
                        </NavItem>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}
