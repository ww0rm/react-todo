import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from "../actions/user";

import { withStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import TextField from "@material-ui/core/TextField";


const styles = theme => ({
    root: {
        flexGrow: 1,
        height: '100%',
    },
    auth: {
        height: '100%',
    },
    paper: {
        padding: theme.spacing.unit * 2,
        height: '100%',
        color: theme.palette.text.secondary,
    },
    control: {
        padding: theme.spacing.unit * 2,
    },
    margin: {
        margin: theme.spacing.unit,
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
});

@connect(state => (state), dispatch => ({
    actions: bindActionCreators(actions, dispatch)
}))
class Auth extends Component {
    constructor () {
        super();
        this.state = {login: '', password: ''};
    };

    login = () => {
        this.props.actions.login({login: this.state.login, password: this.state.password});
    };

    handleLoginChange = (e) => {
        this.setState({login: e.target.value, password: this.state.password});
    };

    handlePasswordChange = (e) => {
        this.setState({login: this.state.login, password: e.target.value});
    };

    logout = () => {
        this.props.actions.logout();
    };

    render() {
        const { classes } = this.props;
        return (
            <Grid container className={classes.root}>
                <Grid item xs={12}>
                    <Grid
                        container
                        spacing={16}
                        className={classes.auth}
                        alignItems="center"
                        justify="center"
                    >
                        <Grid key="0" item>
                            <Paper className={classes.paper}>
                                <TextField
                                    id="name"
                                    label="Name"
                                    className={classes.textField}
                                    value=""
                                    margin="normal"
                                />
                            </Paper>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        );
    }
};

export default withStyles(styles)(Auth);
