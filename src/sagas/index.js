import "regenerator-runtime/runtime";

import { all, fork } from 'redux-saga/effects';
import * as user from "./user";

const forkSagas = sagas => Object.values(sagas).map(saga => fork(saga));

export default function* root() {
    yield all([
        ...forkSagas(user),
    ]);
};