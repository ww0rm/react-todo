import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from './containers/App';
import Auth from './containers/Auth';
import store from './store';
import history from './config/history'
import {Route, Switch} from "react-router-dom";
import { ConnectedRouter } from 'react-router-redux';

ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <Switch>
                <Route exact path='/' component={App}/>
                <Route path='/auth' component={Auth}/>
            </Switch>
        </ConnectedRouter>
    </Provider>,
    document.getElementById('root'));
